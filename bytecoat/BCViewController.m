//
//  BCViewController.m
//  bytecoat
//
//  Created by Standa Musil on 7/7/13.
//  Copyright (c) 2013 logicry. All rights reserved.
//

#import "BCViewController.h"
#import "UIView+BCView.h"

@interface BCViewController ()
{
    NSArray *menuItems;
}
@end

@implementation BCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    menuItems = @[_menuItem1, _menuItem2, _menuItem3];
}

- (IBAction)menuTop:(id)sender {
    [_menuItem1 verticalAlignTopOf:_content.frame withIndex:[menuItems indexOfObject:_menuItem1] inViews:menuItems];
    [_menuItem2 verticalAlignTopOf:_content.frame withIndex:[menuItems indexOfObject:_menuItem2] inViews:menuItems];
    [_menuItem3 verticalAlignTopOf:_content.frame withIndex:[menuItems indexOfObject:_menuItem3] inViews:menuItems];
}

- (IBAction)menuCenter:(id)sender {
    [_menuItem1 verticalAlignCenterOf:_content.frame withIndex:[menuItems indexOfObject:_menuItem1] inViews:menuItems];
    [_menuItem2 verticalAlignCenterOf:_content.frame withIndex:[menuItems indexOfObject:_menuItem2] inViews:menuItems];
    [_menuItem3 verticalAlignCenterOf:_content.frame withIndex:[menuItems indexOfObject:_menuItem3] inViews:menuItems];
}

- (IBAction)menuBottom:(id)sender {
    [_menuItem1 verticalAlignBottomOf:_content.frame withIndex:[menuItems indexOfObject:_menuItem1] inViews:menuItems];
    [_menuItem2 verticalAlignBottomOf:_content.frame withIndex:[menuItems indexOfObject:_menuItem2] inViews:menuItems];
    [_menuItem3 verticalAlignBottomOf:_content.frame withIndex:[menuItems indexOfObject:_menuItem3] inViews:menuItems];
}

- (IBAction)reverseItems:(id)sender {
    menuItems = [[menuItems reverseObjectEnumerator] allObjects];
}

- (IBAction)alignLeft:(id)sender {
    [_box1 alignLeftOf:_box2.frame];
}

- (IBAction)alignCenter:(id)sender {
    [_box1 alignCenterOf:_box2.frame];
}

- (IBAction)alignRight:(id)sender {
    [_box1 alignRightOf:_box2.frame];
}

- (IBAction)valignTop:(id)sender {
    [_box1 verticalAlignTopOf:_box2.frame];
}

- (IBAction)valignCenter:(id)sender {
    [_box1 verticalAlignCenterOf:_box2.frame];
}

- (IBAction)valignBottom:(id)sender {
    [_box1 verticalAlignBottomOf:_box2.frame];
}

- (IBAction)inFrontOf:(id)sender {
    [_box1 moveFrameInFrontOf:_box2.frame withMargin:5];
}

- (IBAction)nextTo:(id)sender {
    [_box1 moveFrameNextTo:_box2.frame withMargin:5];
}

- (IBAction)above:(id)sender {
    [_box1 moveFrameAbove:_box2.frame withMargin:5];
}

- (IBAction)under:(id)sender {
    [_box1 moveFrameUnder:_box2.frame withMargin:5];
}
@end
