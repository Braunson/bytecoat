//
//  main.m
//  bytecoat
//
//  Created by Standa Musil on 7/7/13.
//  Copyright (c) 2013 logicry. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BCAppDelegate class]));
    }
}
