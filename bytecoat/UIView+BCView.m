//
//  UIView+BCView.m
//  bytecoat
//
//  Created by Standa Musil on 7/7/13.
//  Copyright (c) 2013 logicry. All rights reserved.
//

#import "UIView+BCView.h"

@implementation UIView (CVTView)

#pragma mark - Frame movement

- (void)moveFrameByX:(CGFloat)x
{
    self.frame = CGRectMake(self.frame.origin.x + x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
}

- (void)moveFrameByY:(CGFloat)y
{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y + y, self.frame.size.width, self.frame.size.height);
}

- (void)moveFrameToX:(CGFloat)x
{
    self.frame = CGRectMake(x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
}

- (void)moveFrameToY:(CGFloat)y
{
    self.frame = CGRectMake(self.frame.origin.x, y, self.frame.size.width, self.frame.size.height);
}

- (void)moveFrameInFrontOf:(CGRect)frame withMargin:(CGFloat)margin
{
    [self moveFrameToX:frame.origin.x - margin - self.frame.size.width];
}

- (void)moveFrameNextTo:(CGRect)frame withMargin:(CGFloat)margin
{
    [self moveFrameToX:frame.origin.x + frame.size.width + margin];
}

- (void)moveFrameAbove:(CGRect)frame withMargin:(CGFloat)margin
{
    [self moveFrameToY:frame.origin.y - margin - self.frame.size.height];
}

- (void)moveFrameUnder:(CGRect)frame withMargin:(CGFloat)margin
{
    [self moveFrameToY:frame.origin.y + frame.size.height + margin];
}

#pragma mark - Frame modifications

- (void)setFrameOrigin:(CGPoint)origin
{
    self.frame = CGRectMake(origin.x, origin.y, self.frame.size.width, self.frame.size.height);
}

- (void)setFrameSize:(CGSize)size
{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, size.height);
}

- (void)changeFrameWidthBy:(CGFloat)points
{
    [self setFrameSize:CGSizeMake(self.frame.size.width + points, self.frame.size.height)];
}

- (void)changeFrameHeightBy:(CGFloat)points
{
    [self setFrameSize:CGSizeMake(self.frame.size.width, self.frame.size.height + points)];
}

#pragma mark - Frame alignments

- (void)alignLeftOf:(CGRect)frame
{
    [self moveFrameToX:frame.origin.x];
}

- (void)alignCenterOf:(CGRect)frame
{
    CGFloat centerX = frame.origin.x + (frame.size.width / 2.0f);
    [self moveFrameToX:centerX - (self.frame.size.width / 2.0f)];
}

- (void)alignRightOf:(CGRect)frame
{
    [self moveFrameToX:frame.origin.x + frame.size.width - self.frame.size.width];
}

#pragma mark - Frame vertical alignments

- (void)verticalAlignTopOf:(CGRect)frame
{
    [self moveFrameToY:frame.origin.y];
}

- (void)verticalAlignCenterOf:(CGRect)frame
{
    CGFloat centerY = frame.origin.y + (frame.size.height / 2.0f);
    [self moveFrameToY:centerY - (self.frame.size.height/2.0f)];
}

- (void)verticalAlignBottomOf:(CGRect)frame
{
    CGFloat bottomY = frame.origin.y + frame.size.height;
    [self moveFrameToY:(bottomY - self.frame.size.height)];
}

- (void)verticalAlignTopOf:(CGRect)frame withIndex:(NSUInteger)index inViews:(NSArray *)views
{
    if (index >= [views count]) {
        NSLog(@"[%@] Invalid index in frames", [self class]);
        return;
    }
    
    CGFloat indexHeight = 0;
    for (int i = 0; i < index; i++) {
        indexHeight += [(UIView *)views[i] frame].size.height;
    }
    
    [self moveFrameToY:frame.origin.y + indexHeight];
}

- (void)verticalAlignCenterOf:(CGRect)frame withIndex:(NSUInteger)index inViews:(NSArray *)views
{
    if (index >= [views count]) {
        NSLog(@"[%@] Invalid index in frames", [self class]);
        return;
    }
    
    CGFloat indexHeight = 0;
    CGFloat framesHeight = 0;
    int i = 0;
    
    for (UIView *view in views) {
        framesHeight += view.frame.size.height;
        
        if (i < index) {
            indexHeight += view.frame.size.height;
        }
        
        i++;
    }
    
    CGFloat centerY = frame.origin.y + (frame.size.height / 2.0f);
    [self moveFrameToY:centerY - (framesHeight/2.0f) + indexHeight];
}

- (void)verticalAlignBottomOf:(CGRect)frame withIndex:(NSUInteger)index inViews:(NSArray *)views
{
    if (index >= [views count]) {
        NSLog(@"[%@] Invalid index in frames", [self class]);
        return;
    }
    
    CGFloat indexHeight = 0;
    for (int i = 0; i < index; i++) {
        indexHeight += [(UIView *)views[i] frame].size.height;
    }
    
    [self moveFrameToY:frame.origin.y + frame.size.height - indexHeight - self.frame.size.height];
}

#pragma mark - Frame animations

- (void)moveFrameAnimated:(CGRect)frame
                 duration:(double)duration
           finishSelector:(SEL)finish
{
    static int callCounter = 0;
    
    [UIView beginAnimations:[NSString stringWithFormat:@"viewMoveAnimation_%d", callCounter] context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:finish];
    
    self.frame = frame;
    
    [UIView commitAnimations];
}

- (void)moveFrameAnimated:(CGRect)frame duration:(double)duration finishBlock:(void (^)(BOOL))finish
{
    [UIView animateWithDuration:duration
                     animations:^{
                         self.frame = frame;
                     }
                     completion:finish];
}

@end
