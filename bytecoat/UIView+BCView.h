//
//  UIView category for managing frame positioning
//  actions and basic animations
//
//  UIView+BCView.h
//  bytecoat
//
//  Created by Standa Musil on 7/7/13.
//  Copyright (c) 2013 logicry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CVTView)

/*
 * Moves the frame by the given X value
 * x = -10 moves frame by 10 points left
 * x = 10 moves frame by 10 points right
 *
 * @param CGFloat x
 */
- (void)moveFrameByX:(CGFloat)x;

/*
 * Moves the frame by the given Y value
 * y = -10 moves frame by 10 points up
 * y = 10 moves frame by 10 points down
 *
 * @param CGFloat y
 */
- (void)moveFrameByY:(CGFloat)y;

/*
 * Moves the frame to the given X value
 *
 * @param CGFloat x
 */
- (void)moveFrameToX:(CGFloat)x;

/*
 * Moves the frame to the given Y value
 *
 * @param CGFloat y
 */
- (void)moveFrameToY:(CGFloat)y;

/*
 * Moves frame in front of the given frame with specified margin
 *
 * @param CGRect frame
 * @param CGFloat margin
 */
- (void)moveFrameInFrontOf:(CGRect)frame withMargin:(CGFloat)margin;

/*
 * Moves frame next to the given frame with specified margin
 *
 * @param CGRect frame
 * @param CGFloat margin
 */
- (void)moveFrameNextTo:(CGRect)frame withMargin:(CGFloat)margin;

/*
 * Moves frame above the given frame with specified margin
 *
 * @param CGRect frame
 * @param CGFloat margin
 */
- (void)moveFrameAbove:(CGRect)frame withMargin:(CGFloat)margin;

/*
 * Moves frame under the given frame with specified margin
 *
 * @param CGRect frame
 * @param CGFloat margin
 */
- (void)moveFrameUnder:(CGRect)frame withMargin:(CGFloat)margin;

/*
 * Sets the frame origin leaving the same size
 *
 * @param CGPoint origin
 */
- (void)setFrameOrigin:(CGPoint)origin;

/*
 * Sets the frame size leaving the same origin
 *
 * @param CGSize size
 */
- (void)setFrameSize:(CGSize)size;

/*
 * Changes the frame width by the given points
 * f.e. points = -10 reduces the frame width by 10 points
 *
 * @param CGFloat points
 */
- (void)changeFrameWidthBy:(CGFloat)points;

/*
 * Changes the frame height by the given points
 * f.e. points = -10 reduces the frame height by 10 points
 *
 * @param CGFloat points
 */
- (void)changeFrameHeightBy:(CGFloat)points;

/*
 * Aligns to the left of the given frame
 *
 * @param CGRect frame
 */
- (void)alignLeftOf:(CGRect)frame;

/*
 * Aligns to the center of the given frame
 *
 * @param CGRect frame
 */
- (void)alignCenterOf:(CGRect)frame;

/*
 * Aligns to the right of the given frame
 *
 * @param CGRect frame
 */
- (void)alignRightOf:(CGRect)frame;

/*
 * Sets vertical align to the top of the given frame
 *
 * @param CGRect frame
 */
- (void)verticalAlignTopOf:(CGRect)frame;

/*
 * Sets vertical align to the center of the given frame
 *
 * @param CGRect frame
 */
- (void)verticalAlignCenterOf:(CGRect)frame;

/*
 * Sets vertical align to the bottom of the given frame
 *
 * @param CGRect frame
 */
- (void)verticalAlignBottomOf:(CGRect)frame;

/*
 * Sets vertical TOP align to self's frame by the given list
 * of items that should be ordered vertically
 *
 * for example: I have two tabs in menu (tab1 and tab2), and I want to have
 * them both in center of the menu, though they are single views.
 *
 *      TAB1   | MENU |
 *      TAB2   | MENU |
 *             | MENU |
 *             | MENU |
 *
 * First I prepare list of items: NSArray *views = @[tab1, tab2];
 * Then I call [tab1 verticalAlignTopOf:menu.frame withIndex:[views indexOfObject:tab1] inViews:views];
 * To position also tab2: [tab2 verticalAlignTopOf:menu.frame withIndex:[views indexOfObject:tab2] inViews:views];
 */
- (void)verticalAlignTopOf:(CGRect)frame withIndex:(NSUInteger)index inViews:(NSArray *)views;

/*
 * Sets vertical CENTER align to self's frame by the given list
 * of items that should be ordered vertically
 *
 * for example: I have two tabs in menu (tab1 and tab2), and I want to have
 * them both in center of the menu, though they are single views.
 *
 *             | MENU |
 *      TAB1   | MENU |
 *      TAB2   | MENU |
 *             | MENU |
 *
 * First I prepare list of items: NSArray *views = @[tab1, tab2];
 * Then I call [tab1 verticalAlignCenterOf:menu.frame withIndex:[views indexOfObject:tab1] inViews:views];
 * To position also tab2: [tab2 verticalAlignCenterOf:menu.frame withIndex:[views indexOfObject:tab2] inViews:views];
 */
- (void)verticalAlignCenterOf:(CGRect)frame withIndex:(NSUInteger)index inViews:(NSArray *)views;

/*
 * Sets vertical BOTTOM align to self's frame by the given list
 * of items that should be ordered vertically
 *
 * for example: I have two tabs in menu (tab1 and tab2), and I want to have
 * them both in center of the menu, though they are single views.
 *
 *             | MENU |
 *             | MENU |
 *      TAB2   | MENU |
 *      TAB1   | MENU |
 *
 * First I prepare list of items: NSArray *views = @[tab1, tab2];
 * Then I call [tab1 verticalAlignBottomOf:menu.frame withIndex:[views indexOfObject:tab1] inViews:views];
 * To position also tab2: [tab2 verticalAlignBottomOf:menu.frame withIndex:[views indexOfObject:tab2] inViews:views];
 */
- (void)verticalAlignBottomOf:(CGRect)frame withIndex:(NSUInteger)index inViews:(NSArray *)views;

/*
 * Moves view to the given frame
 * This might be mixed up with frame movement
 * helper methods upper
 *
 * @param CGRect frame - destination frame
 * @param double duration - animation dur. in seconds
 * @param SEL finish - finish selector
 */
- (void)moveFrameAnimated:(CGRect)frame
                 duration:(double)duration
           finishSelector:(SEL)finish;

/*
 * Moves view to the given frame
 * This might be mixed up with frame movement
 * helper methods upper
 *
 * @param CGRect frame - destination frame
 * @param double duration - animation dur. in seconds
 * @param (void (^)(BOOL)) finish - finish block
 */
- (void)moveFrameAnimated:(CGRect)frame
                 duration:(double)duration
              finishBlock:(void (^)(BOOL))finish;
@end
