//
//  BCViewController.h
//  bytecoat
//
//  Created by Standa Musil on 7/7/13.
//  Copyright (c) 2013 logicry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *content;

@property (weak, nonatomic) IBOutlet UIView *menuItem1;
@property (weak, nonatomic) IBOutlet UIView *menuItem2;
@property (weak, nonatomic) IBOutlet UIView *menuItem3;

- (IBAction)menuTop:(id)sender;
- (IBAction)menuCenter:(id)sender;
- (IBAction)menuBottom:(id)sender;
- (IBAction)reverseItems:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *box1;
@property (weak, nonatomic) IBOutlet UIView *box2;

- (IBAction)alignLeft:(id)sender;
- (IBAction)alignCenter:(id)sender;
- (IBAction)alignRight:(id)sender;

- (IBAction)valignTop:(id)sender;
- (IBAction)valignCenter:(id)sender;
- (IBAction)valignBottom:(id)sender;

- (IBAction)inFrontOf:(id)sender;
- (IBAction)nextTo:(id)sender;
- (IBAction)above:(id)sender;
- (IBAction)under:(id)sender;

@end
